
# Go Script README :computer: :rocket:

This Go script is a server that handles file uploads.
It loads its configuration from a YAML file.
At first just thourght for getting config files, but nothing stops you to push backups to this. Just be sure to use SSL in this case
Based on an old implementation what was using the deprecated Gorilla/Mux. Let GPT rewrite it with go-chi. So at least 95% GPT written xD. Even theis Readme!


Brakedown:

- It declares global variables including a rate limiter, mutex for synchronization, working directory, log file path, and debug flag.
- The main function is the entry point of the script.
- It calls the setupLogging function to configure logging and create a log file.
- It reads the configuration file using the viper library.
- It retrieves various configuration values such as the server port, SSL certificate path, endpoints, working directory, and debug flag.
- It creates a new router using the gorilla/mux library.
- It sets up a rate limiter for request throttling.
- It iterates over the endpoints defined in the configuration and sets up corresponding handlers for file uploads.
- It sets a custom handler for non-existing endpoints.
- It starts the server and listens for incoming connections.
- If a certificate path is provided, it enables SSL/TLS using http.ListenAndServeTLS; otherwise, it uses http.ListenAndServe.
- The setupLogging function creates a log file in the specified working directory or the current directory if not provided.
- The rateLimit function wraps the next handler with rate limiting functionality.
- The createFile function creates a file at the specified path and returns a file descriptor.
- The uploadHandler function handles file upload requests. It retrieves the file from the request, creates a file at the specified path, and saves the uploaded file to disk.

## :gear: Building


Install Go dependencies:

```shell
go mod download
```

Build and run the server:

```shell
CGO_ENABLED=0 go build -o overview-collector.go
CGO_ENABLED=0 go build -o build/overview-collector.go
```

## :floppy_disk: Configuration

The server's configuration is stored by default in the `config.yaml` file. 
you can overwrite this with the evniroment var `CONFIGFILE` (`CONFIGFILE=myconfig.yaml ./overview-collector.go`)

You can modify the following parameters:

```yaml
server:
  port: "8080" # The port on which the server listens.
  certificate: # Path to the TLS certificate without the ending. +.crt +.key will get apended  (optional). 
  url: "https://foo.biz" # Mostly for logging only. Prints the endpoints at application startup. add port when needed
  workingdir: "./upload" # When not using a full path, The directory where uploaded files are stored.
debug:
  enabled: false # Additional unnamed log lines, possible good when you are not able to write out files

endpoints:
  test/file1.txt: "test/file1" # Relative path, $workingdir/test/file1.txt
  /tmp/upload/test/file2.txt: "test/file2" # Full path, /tmp/upload/test/file2.txt
  /tmp/upload/OpenStack/cloud/network.json: "cloud/OpenStack/network/dc647b97-6d2a-403d-94bf-e56a1071cb4d/d4ce6508-6eac-4e31-9c08-e461c54ca51f" # Resonable secure imo
```

Special, When the path is starting with `backup` you save files with their original name in the path you defined.
This way you can throw over your tared backups. 
Cleaning is up to you. this only recive files  

not fully tested yet e.g. will this make a diffrence? ('' vs '/')
      "backup/blue": "backup/blue"
      "backup/red/": "backup/red"
      
<!-- 
openssl req -x509 -newkey rsa:4096 -keyout /opt/overview-collector/overview-collector.key -out /opt/overview-collector/overview-collector.crt -days 5475 -subj "/CN=configCopy" -nodes 

curl --insecure -X POST -F "file=@/tmt/test" https://127.0.0.1:8080/cloud/OpenStack/network/dc647b97-6d2a-403d-94bf-e56a1071cb4d/d4ce6508-6eac-4e31-9c08-e461c54ca51f
-->

## :rocket: Usage

For example: 
`curl -X POST -F "file=@/tmp/example-file" http://127.0.0.1:8080/cloud/OpenStack/network/dc647b97-6d2a-403d-94bf-e56a1071cb4d/d4ce6508-6eac-4e31-9c08-e461c54ca51f`

`cat ${OUTPUTFILENAME} | curl --insecure -X POST -F "file=@-;filename=OQQ_${start_date}-${end_date}.json" `

Untested: 
`wget --method=POST --body-file=/path/to/file.txt http://localhost:8080/password1`

## :closed_lock_with_key: Rate Limiting

To prevent endpoint enumeration and/or abuse. Requests are limited to 1 request per 3 seconds.



## :warning: Fail2ban 
Not yet tested, pure theory. 
When setup in the dir `/opt/overview-collector`

`/etc/fail2ban/filter.d/overview-collector-non-existing-endpoint.conf`
```
[Definition]
failregex = \d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2} Request for non-existing endpoint from client IP: <HOST>
```


`/etc/fail2ban/jail.local`
```
[overview-collector-non-existing-endpoint]

enabled = true
filter = overview-collector-non-existing-endpoint
logpath = /opt/overview-collector/application.log
maxretry = 3
bantime = 3600
findtime = 600
```


## :dizzy: Installation

First time setup 

```bash
mkdir -p /opt/overview/.tmp
groupadd --gid 9000 --non-unique overview
useradd --no-create-home --shell /usr/bin/bash --home-dir /opt/overview/ --non-unique --uid 9000 --gid 9000 --no-user-group overview

curl -sSL "https://gitlab.com/Underknowledge/GOverview-Collector/-/jobs/artifacts/main/download?job=build" -o /tmp/GOverview-Collector.zip
unzip -j /tmp/GOverview-Collector.zip artifacts/config.yaml -d /opt/overview
unzip -j /tmp/GOverview-Collector.zip artifacts/overview-collector.go -d /tmp/
mv /tmp/overview-collector.go /usr/local/bin/overview-collector.go
chown -R overview:overview /opt/overview


cat <<EOF > /etc/systemd/system/overview-collector.service
# /etc/systemd/system/overview-collector.service
[Unit]
Description=Overview Collector
After=network.target

[Service]

Type=simple
User=overview
Group=overview
ExecStart=/usr/local/bin/overview-collector.go
WorkingDirectory=/opt/overview
Environment=TMPDIR=/opt/overview/.tmp
Restart=always
RestartSec=1s

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable --now overview-collector.service
systemctl status overview-collector.service

journalctl -u overview-collector.service -f
firewall-cmd --permanent --add-port=8080/tcp
firewall-cmd --reload
```

### :passport_control: dummy cert 

quick and dirty selfsinged cert: 
```bash
mkdir -p /opt/overview/.cert
openssl req -x509 -nodes -days 3650 -newkey rsa:4096 -keyout /opt/overview/.cert/overview-collector.key -out /opt/overview/.cert/overview-collector.crt -subj "/CN=arpa.corp"
chown -R overview:overview /opt/overview
```

in the config 
```yaml
server:
  certificate: "/opt/overview/.cert/overview-collector"
```


### :arrow_up: update

```bash
curl -sSL "https://gitlab.com/Underknowledge/GOverview-Collector/-/jobs/artifacts/main/download?job=build" -o /tmp/GOverview-Collector.zip
unzip -j /tmp/GOverview-Collector.zip artifacts/overview-collector.go -d /tmp/
mv /tmp/overview-collector.go /usr/local/bin/overview-collector.go
systemctl restart overview-collector.service
```


<!-- 
`^.* Request for non-existing endpoint from client IP: <HOST>$`
-->






<!-- 


## Painful first setup

GO111MODULE=on go get github.com/spf13/viper
go get github.com/gorilla/mux
go get github.com/spf13/viper



go mod init gitlab.com/Underknowledge/GOverview-Collector
go mod tidy






Write a Golang program where I can upload files with curl or wget. 
The endpoints should be like a password. each server will have a specific endpoint. 
The paths where the files are stored, and the url endpoints to upload the files to should be based on a config file. 
the config file could look like this 
```yaml
server:
  port: "8080"
  certificate: "" 
endpoints:
  password1: "/tmp/test"
  aa436b26-e9a3-4749-b334-bca8e40291e6/dc647b97-6d2a-403d-94bf-e56a1071cb4d/NFS-Config: "./foobar"
```

use for this spf13/viper and gorilla/mux 

please add ratelimiting to it, that youre not able to post more then 1 request every 3 secconds 
use for this time/rate

it should be possible to have paths like this
http://localhost:8080/password1 
http://localhost:8080/aa436b26-e9a3-4749-b334-bca8e40291e6/dc647b97-6d2a-403d-94bf-e56a1071cb4d/NFS-Config 

curl -X POST -F "file=@/etc/fstab" http://localhost:8080/password1


 -->
