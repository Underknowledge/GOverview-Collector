package main

import (
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"golang.org/x/time/rate"
)

var (
	limiter      *rate.Limiter
	workingDir   string
	logFilePath  string
	debugEnabled bool
)

func main() {

	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout})
	configFile := os.Getenv("CONFIGFILE")
	if configFile == "" {
		configFile = "config.yaml"
	}

	viper.SetConfigFile(configFile)
	if err := viper.ReadInConfig(); err != nil {
		log.Info().Msgf(" No or faulty Config file \"%s\" found... \nexample:\n\nserver:\n  port: \"8080\"\n  certificate: \"optional\"\n  workingdir: \"./upload\" # when not using a full path, put files in there. \ndebug:\n  enabled: false\nendpoints:\n  url/for/endpoint1: \"test/example1.txt\" # $workingdir/test/example1.txt\n  url/for/endpoint2: \"other/path/example2.txt\" # $workingdir/other/path/test/example2.txt\n  url/for/endpoint3: \"/tmp/upload3\" # Full path (needs to be writeable by the script user)\n  overview/dc647b97-6d2a-403d-94bf-e56a1071cb4d/secret: \"/full/path/to/file/NFS-Config\"\n  client1/secret/url/to/save/backup: \"/nfs/backups/client1\" # Special, when pre or suffix is backup it behaves like a folder (Keeping original filenames)\n\n\n", configFile)
		log.Fatal().Err(err).Msgf("Failed to read config file")

	}

	setupLogging()

	port := viper.GetString("server.port")
	certificate := viper.GetString("server.certificate")
	endpoints := viper.GetStringMapString("endpoints")
	workingDir = viper.GetString("server.workingdir")
	log.Info().Msgf("workingDir is set to %s", workingDir)
	url := viper.GetString("server.url")
	if url != "" {
		log.Info().Msgf("URL set to %s", url)
	} else {
		url = "127.0.0.1:" + port
		log.Info().Msgf("URL set to %s", url)
	}

	debugEnabled = viper.GetBool("debug.enabled")
	if debugEnabled {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339})
		log.Debug().Msgf("Debug logging enabled")
	} else {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339})
		log.Info().Msgf("Debug logging disabled")
	}

	r := chi.NewRouter()

	// Set up rate limiter
	limiter = rate.NewLimiter(rate.Every(20*time.Second), 3)

	// https://github.com/go-chi/chi/issues/674#issuecomment-962370701 ignore casin on incoming requests
	r.Use(func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			log.Info().
				Str("method", r.Method).
				Str("url", r.URL.Path).
				Str("remote_addr", r.RemoteAddr).
				Int64("content_length", r.ContentLength).
				Msg("Incoming request")
			r.URL.Path = strings.ToLower(r.URL.Path)

			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	})

	for secreturl, definedpath := range endpoints {
		if !filepath.IsAbs(definedpath) {
			// Prepend workingDir to the relative path
			definedpath = filepath.Join(workingDir, definedpath)
		}
		log.Info().Msgf("Setup of %s/%s saved at %s", url, secreturl, definedpath)
		r.Route(fmt.Sprintf("/%s", secreturl), func(r chi.Router) {

			r.Use(rateLimit)
			// r.Post("/", uploadHandler(path, secretURL))
			r.Post("/", uploadHandler(secreturl, definedpath))
		})
	}

	// Handler for non-existing endpoints
	r.NotFound(func(w http.ResponseWriter, r *http.Request) {
		ip, _, err := net.SplitHostPort(r.RemoteAddr)
		if err != nil {
			log.Info().Msgf("Failed to get client IP address: %v", err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
		log.Info().Msgf("Request for non-existing endpoint from client IP: %s", ip)
		http.NotFound(w, r)
	})

	log.Info().Msgf("Server listening on port %s", port)
	log.Info().Msgf("Usage: \n  curl --insecure -X POST -F \"file=@/path/to/file\" %s/url/path", url)

	var err error
	if certificate != "" {
		log.Info().Msgf("Using SSL")
		err = http.ListenAndServeTLS(fmt.Sprintf(":%s", port), certificate+".crt", certificate+".key", r)
	} else {
		err = http.ListenAndServe(fmt.Sprintf(":%s", port), r)
	}

	if err != nil {
		// log.Fatal("Server error: %v", err)
		log.Fatal().Err(err).Msg("Server error")

	}
}

func setupLogging() {
	// Create a log file in the working directory
	zerolog.SetGlobalLevel(zerolog.InfoLevel)

	logFileName := "application.log"
	if workingDir != "" {
		logFilePath = filepath.Join(workingDir, logFileName)
	} else {
		cwd, err := os.Getwd()
		if err != nil {
			// log.Fatal("Failed to get current working directory: %v", err)
			log.Fatal().Err(err).Msg("Failed to get current working directory")
		}
		logFilePath = filepath.Join(cwd, logFileName)
	}

	// Open the log file in append mode
	logFile, err := os.OpenFile(logFilePath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		// log.Fatal("Failed to open log file: %v", err)
		log.Fatal().Err(err).Msg("Failed to open log file")

	}
	// , TimeFormat: time.RFC3339
	// Set the log output to the log file
	log.Logger = zerolog.New(logFile).With().Timestamp().Logger()
}

func rateLimit(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if limiter.Allow() {
			next.ServeHTTP(w, r)
		} else {
			ip, _, err := net.SplitHostPort(r.RemoteAddr)
			if err != nil {
				log.Info().Msgf("rateLimit, Failed to get client IP address: %v", err)
				http.Error(w, "Internal Server Error. your client is not behaving as expected", http.StatusInternalServerError)
				return
			}
			log.Info().Msgf("Rate limit exceeded for client IP: %s", ip)
			http.Error(w, "Rate limit exceeded", http.StatusTooManyRequests)
		}
	})
}

func createFile(pathcreatefile string, jobID string) (*os.File, error) {
	dir, file := filepath.Split(pathcreatefile) // Extract the directory and file name from the pathcreatefile

	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		return nil, err
	}
	// log.Debug().Msgf("createFile %s, %s", dir, file)

	log.Debug().Msgf("[%s] Creating/truncateing file: %s/%s", jobID, dir, file)

	return os.Create(dir + file)
}

// TODO: ??? Change var path, to less descriptive
// r.Post("/", uploadHandler(path, secretUrl)) WAS

// is
// r.Post("/", uploadHandler(secretUrl, definedPath))
// func uploadHandler(path string, endpoint string) http.HandlerFunc {
func uploadHandler(urlEndpoint string, uploadDefinedPath string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		jobID := time.Now().Format("20060102150405") // Generate job ID based on current timestamp
		ip, _, err := net.SplitHostPort(r.RemoteAddr)
		if err != nil {
			log.Fatal().Msgf("[%s] uploadHandler, Failed to get client IP address: %v", jobID, err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
		log.Info().Msgf("[%s] Accepting new Job from client IP address: %v path: %s", jobID, ip, uploadDefinedPath)

		err = r.ParseMultipartForm(32 << 20) // Limit the maximum memory to 32MB
		if err != nil {
			log.Error().Msgf("[%s] Error ParseMultipartForm: %s", jobID, uploadDefinedPath)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		file, handler, err := r.FormFile("file")
		if err != nil {
			log.Error().Msgf("[%s] Failed at handler: %s", jobID, handler)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		defer file.Close()

		filename := filepath.Base(handler.Filename)
		filename = strings.ReplaceAll(filename, " ", "_") // Replace spaces in the filename with underscores

		log.Debug().Msgf("[%s] Writing file: %s", jobID, filename)

		currentpath := uploadDefinedPath
		if strings.HasSuffix(urlEndpoint, "backup") || strings.HasPrefix(urlEndpoint, "backup") {
			// Use the original filename
			currentpath = filepath.Join(uploadDefinedPath, handler.Filename)
		}

		// savePath := filepath.Join(path, filename)
		f, err := createFile(currentpath, jobID)
		if err != nil {
			log.Error().Msgf("[%s] Failed at createFile: %s", jobID, currentpath)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer f.Close()

		_, err = io.Copy(f, file)
		if err != nil {
			log.Error().Msgf("[%s] Failed at io.Copy: %s", jobID, currentpath)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer r.MultipartForm.RemoveAll()

		log.Info().Msgf("[%s] Job finished, file saved at: %s", jobID, currentpath)
		w.WriteHeader(http.StatusOK)
	}
}
